"use strict";

const mutantService = require('../services/mutantService'),
			MutantDB    	= require('../../../database/database.js'),
		  mutantDB    	= new MutantDB(process.env.DB_PATH),
			redis    			= require("redis"),
			rclient 		  = redis.createClient(6379, '0.0.0.0');

module.exports = {
	async checkMutant (req, res) {
    let dna = req.body.dna;
    // dna validation
    if (!dna || typeof dna !== 'object') {
      res.status(401).send({
        "error": "Access denied, missing dna array param"
      });
    }
    let response = await mutantService.isMutant(dna);
    if (response) {
      res.status(200).send({
        "message": "Mutant found!"
      });
    } else {
      res.status(403).send({
        "message": "Mutant not found :(, keep on trying magneto"
      });
    }
	},
	async statistics (req, res) {
		await rclient.get('statistics', async function (err, data) {
			if(err || data === null) {
				let result = await mutantDB.run(`
					select
					SUM(CASE WHEN isMutant = true THEN 1 ELSE 0 END) as count_mutant_dna,
					SUM(CASE WHEN isMutant = false THEN 1 ELSE 0 END) as count_human_dna
					from verified_dna
					`);
					let ratio = 0;
					if(result[0].count_mutant_dna > 0 && result[0].count_human_dna > 0) {
						ratio = result[0].count_mutant_dna / result[0].count_human_dna;
					}
					result = {
						"count_mutant_dna": result[0].count_mutant_dna,
						"count_human_dna": result[0].count_human_dna,
						"ratio": ratio
					};
					await rclient.set('statistics', JSON.stringify(result), 'EX', 60);
					res.status(200).send(result);
			} else {
				res.status(200).send(JSON.parse(data));
			}
		});
	}
}
