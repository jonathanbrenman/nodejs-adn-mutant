"use strict";

module.exports = {
	root(req, res) {
		res.status(200).send({
		  info: 'ML Mutants',
		  environment: process.env.ENVIRONMENT,
		  version: 'V1'
		});
	}
}
