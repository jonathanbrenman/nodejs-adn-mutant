"use strict";

const MutantDB    = require('../../../database/database.js'),
		  mutantDB    = new MutantDB(process.env.DB_PATH);

module.exports = {
	async isMutant(dna) {
    let mutant_block   = await this.checkBlocks(dna);
		//console.log(mutant_block)
		let isMutantResult = mutant_block.length > 1;
		let db_insert    = await mutantDB.db.run(`insert into verified_dna(dna, isMutant) values('${JSON.stringify(dna)}',${isMutantResult});`);
    return isMutantResult;
	},
  async checkBlocks (dna) {
    let horizontal = await this.mutantChecker(dna);
		let vertical   = await this.mutantChecker(this.getVerticals(dna));
    let diagonal   = await this.mutantChecker(this.getDiagonals(dna));
    let inverse    = await this.mutantChecker(this.getInverseDiagonals(dna));
    return horizontal.concat(vertical).concat(diagonal).concat(inverse);
  },
  getDiagonals (matrix) {
    // Diagonal
		var a, x, y;
		var z, output = [];
		for (a = 0; a < matrix.length; a++) {
		  z = [];
		  for(y = a, x = 0; y >= 0; y--, x++) {
				z.push(matrix[y][x]);
				output.push(z);
			}
		}
		for (a = 1; a < matrix[0].length; a++) {
		  z = [];
		  for(y = matrix.length - 1, x = a; x < matrix[0].length; y--, x++) {
				z.push(matrix[y][x]);
				output.push(z);
			}
		}
		let unique = [...new Set(output)];
		return unique.map((array) => {
	  	return array.join('');
    });
  },
  getInverseDiagonals (matrix) {
    // Diagonal invert
		let invert = matrix.map((block) => {
      return block.split("").reverse().join("");
    });
		return this.getDiagonals(invert);
  },
	getVerticals (matrix) {
		var x, y, a = 0;
		var z, output = [];
		for (x = 0; x < matrix.length; x++) {
			z = [];
			for(y = a; y < matrix.length; y++) {
				z.push(matrix[y][x]);
				output.push(z);
			}
		}
		let unique = [...new Set(output)];
		return unique.map((array) => {
			return array.join('');
		});
	},
  mutantChecker (dna) {
    let regex  = /([ATCG])\1{3,4}/;
    return dna.filter((block) => {
      return regex.test(block);
    });
  }
}
