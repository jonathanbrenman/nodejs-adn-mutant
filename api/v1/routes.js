
module.exports = (app) => {

		// Agrego Controladores
		const index_controller  = require('./controllers/indexController');
    const mutant_controller = require('./controllers/mutantController');

		// MUTANT
		app.get('/', index_controller.root);
		app.post('/mutant', mutant_controller.checkMutant);
		app.get('/stats', mutant_controller.statistics);

	  // NOT FOUND 404
	  app.use(function (req, res, next) {
	  	res.status(404).send({
	  		error: "Not found."
		});
	});

};
