# nodeJS-adn-mutant

# Setup

1) nvm use 11.9.0 or nvm install 11.9.0
2) npm install
3) create .env file in the app root with variables:
   ENVIRONMENT=development
   APP_HOST=0.0.0.0
   APP_PORT=9320
   DB_PATH='./database/db.sqlite'
4) npm start

# Details
- Settings -> .env file (host, port, environment).
- API uses prefix /api/v1/

# Deployed

AWS free tier - EC2 t2.micro
http://3.215.77.219:9320/api/v1/mutant
http://3.215.77.219:9320/api/v1/stats
