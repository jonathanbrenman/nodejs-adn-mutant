"use strict";

const sqlite3 = require('sqlite3');

class MutantDB {
  constructor (dbFilePath) {
    this.db = new sqlite3.Database(dbFilePath, (err) => {
      if (err) {
        console.log('Could not connect to database', err);
      } else {
        //console.log('Connected to database');
        this.createTable();
      }
    });
  }
  createTable () {
    const sql = `CREATE TABLE IF NOT EXISTS verified_dna (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                dna TEXT,
                isMutant BOOLEAN)`;
    return this.db.run(sql);
  }
  run(query) {
    var that = this;
    return new Promise(function(resolve, reject) {
      that.db.all(query, [], function(err, rows)  {
          if (err) {
            reject("Read error: " + err.message);
          }else {
            resolve(rows);
          }
      });
    });
  }
}

module.exports = MutantDB;
