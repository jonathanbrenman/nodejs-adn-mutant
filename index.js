/*
	Author Jonathan Brenman
	App index.js
*/

// Levanto variables de entorno en file .env
require('dotenv').config();
// Requires aqui van las dependencias y librerias
const http    	  = require('http'),
      express     = require('express'),
      bodyParser  = require('body-parser'),
      app 		    = express(),
      cors        = require('cors'),
      MutantDB    = require('./database/database.js'),
      mutantDB    = new MutantDB(process.env.DB_PATH);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());
app.options(cors());


const v1 = express.Router();
app.use('/api/v1', v1);

// Agrego Rutas versionadas
require('./api/v1/routes')(v1);

app.use(function (req, res, next) {
  	res.status(404).send({
  		error: "The route not found, please try another.",
      info: "API over --> /api/v1"
	});
});

const server = http.createServer(app);

server.listen(process.env.APP_PORT, process.env.APP_HOST, () => {
	console.log('------------------------------------------------------------');
	console.log('Mutant API Server Started..');
	console.log(`ENVIRONMENT      =>>> ${process.env.ENVIRONMENT}`);
	console.log(`API              =>>> http://${process.env.APP_HOST}:${process.env.APP_PORT}`);
	console.log('------------------------------------------------------------');
});
